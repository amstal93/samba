#!/bin/bash

# Find out where we're running from
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! "$(pwd)" == "${script_dir}" ]; then
  echo "Not allowed. First execute:"
  echo "cd ${script_dir}"
  echo " Then try again."
  exit 1
fiexit 1
fi

cmd="${1}"

git fetch origin master
git reset --hard FETCH_HEAD
git clean -df
git pull

if [ "${cmd}" == "--all" ]; then
  "${script_dir}"/build.sh || exit 1
  "${script_dir}"/stop.sh
  "${script_dir}"/run.sh || exit 1
fi
