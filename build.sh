#!/bin/bash

# Find out where we're running from
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! "$(pwd)" == "${script_dir}" ]; then
  echo "Not allowed. First execute:"
  echo "cd ${script_dir}"
  echo " Then try again."
  exit 1
fi

# shellcheck disable=SC1090
source "${script_dir}/repo_config.txt"

# build a local image
# shellcheck disable=SC2154
docker build --rm -t "${image}" . || exit 1

# copy the configuration file to a user-managed location.
if [ ! -f "${HOME}/.config/docker/smb.conf" ]; then
  mkdir -p "${HOME}/.config/docker/"
  cp "${script_dir}/container/default.conf" "${HOME}/.config/docker/smb.conf"
fi

# create a user configuration pointing at the storage location
# shellcheck disable=SC2154
if [ ! -f "${HOME}/.config/docker/${container}-store.txt" ]; then
  mkdir -p "${HOME}/.config/docker/"
  echo "store=${store}" > "${HOME}/.config/docker/${container}-store.txt"
fi
